<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \DB::table('users')->delete();

        \DB::table('users')->insert([
            0 => [
                'id' => '1',
                'name' => 'Официант',
                'phone' => '79051112233',
                'password' => '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq',
                'remember_token' => 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46',
                'deleted_at' => NULL
            ],
            1 => [
                'id' => '2',
                'name' => 'Люся',
                'phone' => '79052223344',
                'password' => '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq',
                'remember_token' => 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46',
                'deleted_at' => NULL
            ],
            2 => [
                'id' => '3',
                'name' => 'Миша',
                'phone' => '79053334455',
                'password' => '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq',
                'remember_token' => 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46',
                'deleted_at' => NULL
            ],
        ]);
    }

}
