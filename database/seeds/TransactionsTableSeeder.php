<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \DB::table('transactions')->delete();

        \DB::table('transactions')->insert([
            0 => [
                'id' => 1,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 300,
                'check' => 500,
                'diff' => 75,
                'created_at' => '2016-07-11 08:12:16',
                'updated_at' => '2016-07-11 08:12:16'
            ],
            1 => [
                'id' => 2,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 375,
                'check' => 300,
                'diff' => -150,
                'created_at' => '2016-07-11 08:12:27',
                'updated_at' => '2016-07-11 08:12:27'
            ],
            2 => [
                'id' => 3,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 225,
                'check' => 600,
                'diff' => 90,
                'created_at' => '2016-07-11 08:13:21',
                'updated_at' => '2016-07-11 08:13:21'
            ],
            3 => [
                'id' => 4,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 315,
                'check' => 250,
                'diff' => -125,
                'created_at' => '2016-07-11 08:14:18',
                'updated_at' => '2016-07-11 08:14:18'
            ],
            4 => [
                'id' => 5,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 190,
                'check' => 700,
                'diff' => 105,
                'created_at' => '2016-07-11 08:14:51',
                'updated_at' => '2016-07-11 08:14:51'
            ],
            5 => [
                'id' => 6,
                'client_id' => 2,
                'manager_id' => 1,
                'discount' => 295,
                'check' => 400,
                'diff' => 60,
                'created_at' => '2016-07-11 08:15:54',
                'updated_at' => '2016-07-11 08:15:54'
            ]
        ]);
    }

}
