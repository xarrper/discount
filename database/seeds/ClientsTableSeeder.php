<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \DB::table('clients')->delete();

        \DB::table('clients')->insert([
            0 => [
                'id' => '1',
                'user_id' => '2',
                'name' => 'Люсьена Сергеевна',
                'ident' => '6beb7245978193cb3dfddaad03c66424',
                'discount' => '0',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46'
            ],
            1 => [
                'id' => '2',
                'user_id' => '3',
                'name' => 'Михаил Магамедов',
                'ident' => '11a57ebd4dac0e59283fda004d93440e',
                'discount' => '300',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46'
            ]
        ]);
    }

}
