<?php

use Illuminate\Database\Seeder;

class ManagersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \DB::table('managers')->delete();

        \DB::table('managers')->insert([
            0 => [
                'id' => '1',
                'user_id' => '1',
                'name' => 'Толик',
                'description' => 'Может унести за раз до 6 бокалов',
                'created_at' => '2016-03-03 07:33:53',
                'updated_at' => '2016-03-31 01:57:46'
            ]
        ]);
    }

}
