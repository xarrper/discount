<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Pages;
use Tests\CommonHelper;
use App\Http\Domain\IdentService;
use Tests\DataHelper;

class IdentTest extends TestCase {
    
    use DatabaseTransactions;
    use Pages;
    use CommonHelper;
    use DataHelper;
    use WithoutMiddleware;
    
    /**
     * тест на проверку генерации ident клиента при авторизации пользователя
     */
    public function testGeneratIdentClient() {
        $this->authUser($this->userId['client1']);
        $identService = new IdentService();
        $ident = $identService->generatIdent($this->clientId['client1']);
        $this->seeInDatabase('clients', ['ident' => $ident, 'id' => $this->clientId['client1']]);
    }
    
    /**
     * тест на проверку ident на странице клиента, и переход по этой ссылке для менеджера
     */
    public function testDoToIdentForManager() {        
        $this->authUser($this->userId['client1']);
        $identService = new IdentService();
        $ident = $identService->generatIdent($this->clientId['client1']);
        $this->visit($this->urlClientPage())
            ->see($this->urlManagerIdentPage($ident));
        $this->authUser($this->userId['manager']);
        $this->visit($this->urlManagerIdentPage($ident))
            ->see('Люсьена Сергеевна');        
    }
    
}
