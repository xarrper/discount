<?php

namespace Tests;

use Tests\TestCase;
use Tests\Pages;
use Tests\CommonHelper;
use Tests\DataHelper;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PermissionsRouteTest extends TestCase {

    use DatabaseTransactions;
    use Pages;
    use CommonHelper;
    use DataHelper;
    
    /**
     * тест на url /client
     */
    public function testClientShow() {
        $this->permissionGetRoute(                
            $this->urlClientPage(), 
            $this->userId['client1'], 
            $this->userId['manager']
        );
    }
    
    /**
     * тест на url /manager
     */
    public function testManagerShow() {
        $this->permissionGetRoute(
            $this->urlManagerPage(), 
            $this->userId['manager'], 
            $this->userId['client1']
        );
    }
    
    /**
     * тест на url /manager/client/{id}
     */
    public function testClientShowId() {
        $this->permissionGetRoute(
            $this->urlManagerClientIdPage($this->userId['client1']), 
            $this->userId['manager'], 
            $this->userId['client1']
        );
    }
    
    /**
     * тест на url /client/history
     */
    public function testClientHistory() {
        $this->permissionGetRoute(                
            $this->urlClientHistoryPage(), 
            $this->userId['client1'], 
            $this->userId['manager']
        );
    }
    
    /**
     * тест на url /manager/history
     */
    public function testManagerHistory() {
        $this->permissionGetRoute(
            $this->urlManagerHistoryPage(), 
            $this->userId['manager'], 
            $this->userId['client1']
        );
    }
    
    /**
     * тест на url /manager/discount
     */
    public function testDiscountShow() {
        $this->permissionGetRoute($this->urlDiscountPage([
            'check' => '500', 
            'phone' => '79052223344'
        ]), $this->userId['manager'], $this->userId['client1']);
    }
    
    /**
     * тест на url /manager/discount/increase
     */
    public function testDiscountIncrease() {
        $this->permissionpPostRoute($this->urlDiscountCalculatePage([
            'id' => 2, 
            'check' => 500,
            'type' => true
        ]), $this->userId['manager'], $this->userId['client1']);
    }
    
    /**
     * тест на url /manager/discount/decrease
     */
    public function testDiscountDecrease() {
        $this->permissionpPostRoute($this->urlDiscountCalculatePage([
            'id' => 2, 
            'check' => 500,
            'type' => false
        ]), $this->userId['manager'], $this->userId['client1']);
    }
    
    /**
     * тест на url /manager/ident/{ident}
     */
    public function testManagerIdent() {
        $ident = '6beb7245978193cb3dfddaad03c66424';
        $this->permissionGetRoute($this->urlManagerIdentPage($ident), 
                $this->userId['manager'], $this->userId['client1'], true);
    }
    
}
