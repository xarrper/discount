<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Pages;
use Tests\DataHelper;

class AuthTest extends TestCase {
    
    use DatabaseTransactions;
    use Pages;
    use DataHelper;
    use WithoutMiddleware;
    
    /**
     * тест на проверку авторизации пользователя
     */
    public function testAuthUser() {
        $this->visit($this->urlAuthTestPage())->dontSee('79052223344');        
        $this->visit($this->urlAuthEnterPage($this->userId['client1'])); 
        $this->visit($this->urlAuthTestPage())->see('79052223344');
        $this->visit($this->urlAuthExitsPage());
        $this->visit($this->urlAuthTestPage())->dontSee('79052223344');
    }

}
