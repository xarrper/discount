<?php

namespace Tests;

trait Pages {    
    
    public function urlManagerPage() {
        return route("manager.show");
    }
    
    public function urlClientPage() {
        return route("client.show");
    }
    
    public function urlManagerClientIdPage($id) {
        return route("manager.client", ['id' => $id]);
    }
    
    public function urlManagerHistoryPage() {
        return route("manager.history");
    }
    
    public function urlClientHistoryPage() {
        return route("client.history");
    }
    
    public function urlDiscountPage($data) {
        return route("manager.discount.show", $data);
    }
  
    public function urlDiscountCalculatePage($data) {
        return route("manager.discount.calculate", $data);
    }
    
    public function sendSellerForm($data) {
        return $this->type($data['phone'], 'phone')
            ->press('Поиск');
    }
    
    public function sendManagerClientForm() {
        return $this->press('Поиск');
    }
    
    public function urlAuthEnterPage($id) {
        return route("auth.enter", ['id' => $id]);
    }
    
    public function urlAuthExitsPage() {
        return route("auth.exits");
    }
    
    public function urlAuthTestPage() {
        return route("auth.test");
    }
    
    public function urlManagerIdentPage($ident) {
        return route("manager.ident", ['ident' => $ident]);
    }
}