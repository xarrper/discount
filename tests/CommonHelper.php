<?php

namespace Tests;

trait CommonHelper {    

    /**
     * Проверка нахождения массива эелемнтов на странице
     * @param type $data - массив элементов на странице
     */
    public function allSee($data) {
        foreach($data as $content) {
            $this->see($content);
        }
    }
    
    /**
     * авторизация пользователя
     * @param type $id - пользователя
     */
    public function authUser($id) {
        $this->visit($this->urlAuthEnterPage($id));
    } 
    
    /**
     * разлогинивание пользователя
     */
    public function logoutUser() {
        $this->visit($this->urlAuthExitsPage());
    }
    
    /**
     * Проверка на доступ к get route: 
     * 1. пользователь авторизован
     * 2. права доступа пользователя
     * @param type $url - адрес
     * @param type $userId - id пользователя, который может просматривать данную страницу
     * @param type $badUserId - id пользователя, который не имеет права на данную страницу 
     * @param type $redirect - флаг присутствия редиректа в методе
     */
    public function permissionGetRoute($url, $userId, $badUserId, $redirect = false) {
        $this->call('GET', $url);
        $this->assertResponseStatus($this->statusCode['noAuth']);
        
        $this->authUser($userId);
        $this->call('GET', $url);
        if ($redirect) {
            $this->assertResponseStatus($this->statusCode['302']);
            $this->assertRedirectedTo($this->urlManagerClientIdPage(1));
            $this->followRedirects();
        }        
        $this->assertResponseStatus($this->statusCode['ok']);
        $this->logoutUser();
        
        $this->authUser($badUserId);
        $this->call('GET', $url);
        $this->assertResponseStatus($this->statusCode['noRight']);
        $this->logoutUser();
    }
    
    /**
     * Проверка на доступ к post route: 
     * 1. пользователь авторизован
     * 2. права доступа пользователя 
     * @param type $url - адрес
     * @param type $userId - id пользователя, который может просматривать данную страницу
     * @param type $badUserId - id пользователя, который не имеет права на данную страницу
     */
    public function permissionpPostRoute($url, $userId, $badUserId) {
        $this->call('POST', $url);
        $this->assertResponseStatus($this->statusCode['noAuth']);
        
        $this->authUser($userId);
        $this->call('POST', $url);         
        $this->assertResponseStatus($this->statusCode['302']);
        $this->assertRedirectedTo($this->urlManagerPage());
        $this->followRedirects();        
        $this->assertResponseStatus($this->statusCode['ok']);
        $this->logoutUser();
        
        $this->authUser($badUserId);
        $this->call('POST', $url);
        $this->assertResponseStatus($this->statusCode['noRight']);
        $this->logoutUser();
    }
    
    /**
     * Функция просмотра содержимого на странице
     * @param type $userId - id пользователя
     * @param type $url - url страницы
     * @param type $see - что должно отбразиться на странице
     */
    public function checkShowPage($userId, $url, $see) {
        $this->authUser($userId);
        $this->visit($url);
        $this->see($see);
        $this->logoutUser();
    }
    
}
