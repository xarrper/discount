<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Pages;
use Tests\CommonHelper;
use Tests\DataHelper;

class UserTest extends TestCase {
    
    use DatabaseTransactions;
    use Pages;
    use DataHelper;
    use CommonHelper;
    use WithoutMiddleware;
    
    /**
     * Тест на отображение страницы клиента
     */
    public function testShowClient() {
        $this->checkShowPage($this->userId['client1'], $this->urlClientPage(), "Люсьена Сергеевна : 0");
        $this->checkShowPage($this->userId['client2'], $this->urlClientPage(), "Михаил Магамедов : 300");
    }
    
    /**
     * Тест на отображение страницы менеджера
     */
    public function testShowManager() {
        $this->checkShowPage($this->userId['manager'], $this->urlManagerPage(), "Толик");
    }
    
    /**
     * Проверка существование всех полей у формы, и переход на роут
     */
    //todo: информат
//    public function testClickButton() {
//        $this->authUser($this->userId['manager']);
//        $this->visit($this->urlManagerPage())
//            ->sendSellerForm($this->formForDiscount)
//            ->seePageIs($this->urlDiscountPage($this->formForDiscount));
//        $this->logoutUser();
//    }
    
    /**
     * Поиск заказчика
     */
    //todo: авторизация $this->authManager
    public function testFindClient() {
        $this->authUser($this->userId['manager']);
        $this->visit($this->urlManagerPage())
            ->sendSellerForm($this->formForDiscount)
            ->allSee(['Люсьена Сергеевна', '0', 'Cписать', 'Добавить']) 
            ->seePageIs($this->urlDiscountPage($this->formForDiscount));
        $this->logoutUser();
    }   
    
    /**
     * списать баллы, добавить баллы.
     */
    //todo: js, как проверить js
    public function testAvailabilityButtonPoints() {
        $this->authUser($this->userId['manager']);
        $this->visit($this->urlManagerPage())
            ->sendSellerForm($this->formForDiscount);
//            ->allSee(['75', '0']);
        $this->logoutUser();
    }
    
    /**
     * функциональный тест по проверке добавления баллов 
     */
    public function testDiscountIncrease() {
        $this->authUser($this->userId['manager']); 
        $this->seeInDatabase('clients', ['discount' => 300, 'id' => $this->clientId['client2']]);
        $this->call('POST', $this->urlDiscountCalculatePage([
            'id' => $this->clientId['client2'], 
            'check' => 500,
            'increase' => true
        ]));
        $this->seeInDatabase('clients', ['discount' => 375, 'id' => $this->clientId['client2']]);
        $this->seeInDatabase('transactions', [
            'client_id' => $this->clientId['client2'], 
            'manager_id' => $this->managerId['manager'], 
            'discount' => 300, 
            'check' => 500, 
            'diff' => 75
        ]);
        $this->logoutUser();        
    }
    
    /** 
     * функциональный тест по проверке вычитания баллов 
     */
    public function testDiscountDecrease() {
        $this->authUser($this->userId['manager']);
        $this->seeInDatabase('clients', ['discount' => 300, 'id' => $this->clientId['client2']]);
        $this->call('POST', $this->urlDiscountCalculatePage([
            'id' => $this->clientId['client2'], 
            'check' => 500,
            'decrease' => true
        ]));
        $this->seeInDatabase('clients', ['discount' => 150, 'id' => $this->clientId['client2']]);
        $this->seeInDatabase('transactions', [
            'client_id' => $this->clientId['client2'], 
            'manager_id' => $this->managerId['manager'], 
            'discount' => 300, 
            'check' => 500, 
            'diff' => -150
        ]);
        $this->logoutUser();
    }
    
    /**
     * тест на проверку отображения страницы клиента для менеджераа
     */
    public function testManagerClientId() {
        $this->authUser($this->userId['manager']);
        $this->visit($this->urlManagerClientIdPage($this->clientId['client1']));
        $this->allSee(['Люсьена Сергеевна']);
        $this->logoutUser();
    }
    
    /**
     * тест на проверку перехода на страницу discount от страницы клиента для менеджера
     */
    public function testManagerClientClickButton() {
        $this->authUser($this->userId['manager']);
        $this->visit($this->urlManagerClientIdPage($this->clientId['client1']))
            ->sendManagerClientForm()
            ->seePageIs($this->urlDiscountPage([
                'id' => $this->clientId['client1']                
            ]));
        $this->logoutUser();
    }
            
    //TODO дописать тесты
    /**
     * 1. тест на middlewareAuth, 
     * 2. тест на DiscountService, 
     * 3. тест на IdentServis, 
     * 4. тест на js
     */
}
