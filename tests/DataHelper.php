<?php

namespace Tests;

trait DataHelper {    
    //todo: magic method!
    public $statusCode = [
        'ok' => 200,
        'noAuth' => 302,
        'noRight' => 403,
        '302' => 302
    ];
    
    public $userId = [
        'manager' => 1,
        'client1' => 2,
        'client2' => 3        
    ];
    
    public $clientId = [
        'client1' => 1,
        'client2' => 2       
    ];
    
    public $managerId = [
        'manager' => 1     
    ];    
    
    /**
     * данные для формы: check - сумма клиента, phone - номер клиента
     * @var type 
     */
    //TODO - название для переменной
    public $formForDiscount = [
        'phone' => '79052223344'
    ];
    
}
