-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 03 2018 г., 14:08
-- Версия сервера: 5.5.46-0ubuntu0.14.04.2
-- Версия PHP: 5.6.20-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `discount`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ident` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `user_id`, `name`, `ident`, `discount`, `created_at`, `updated_at`) VALUES
(1, 2, 'Люсьена Сергеевна', '6beb7245978193cb3dfddaad03c66424', 0, '2016-03-03 03:33:53', '2016-03-30 21:57:46'),
(2, 3, 'Михаил Магамедов', '11a57ebd4dac0e59283fda004d93440e', 300, '2016-03-03 03:33:53', '2016-03-30 21:57:46');

-- --------------------------------------------------------

--
-- Структура таблицы `managers`
--

CREATE TABLE IF NOT EXISTS `managers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `managers`
--

INSERT INTO `managers` (`id`, `user_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Толик', 'Может унести за раз до 6 бокалов', '2016-03-03 03:33:53', '2016-03-30 21:57:46');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_06_055732_create_clients_table', 1),
('2016_07_06_055818_create_managers_table', 1),
('2016_07_07_060546_create_transactions_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `discount` int(11) NOT NULL COMMENT 'Текущий у пользователя',
  `check` int(11) NOT NULL,
  `diff` int(11) NOT NULL COMMENT 'diff положительный - increase, отрицательный - decrease',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `transactions`
--

INSERT INTO `transactions` (`id`, `client_id`, `manager_id`, `discount`, `check`, `diff`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 300, 500, 75, '2016-07-11 04:12:16', '2016-07-11 04:12:16'),
(2, 2, 1, 375, 300, -150, '2016-07-11 04:12:27', '2016-07-11 04:12:27'),
(3, 2, 1, 225, 600, 90, '2016-07-11 04:13:21', '2016-07-11 04:13:21'),
(4, 2, 1, 315, 250, -125, '2016-07-11 04:14:18', '2016-07-11 04:14:18'),
(5, 2, 1, 190, 700, 105, '2016-07-11 04:14:51', '2016-07-11 04:14:51'),
(6, 2, 1, 295, 400, 60, '2016-07-11 04:15:54', '2016-07-11 04:15:54');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Официант', '79051112233', '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq', 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR', '2016-03-03 03:33:53', '2016-03-30 21:57:46', NULL),
(2, 'Люся', '79052223344', '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq', 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR', '2016-03-03 03:33:53', '2016-03-30 21:57:46', NULL),
(3, 'Миша', '79053334455', '$2y$10$2UArmwoNtyWEqtz70B/VvOHy1XPf6wMjZXFgOAWrMN8KS0HIbWsDq', 'yM63KNncjRITMR19wbNhQnuBC4oCAtnbHN63KkLuh4pZYompQapGFCEHtaiR', '2016-03-03 03:33:53', '2016-03-30 21:57:46', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
