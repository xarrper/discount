/**
 * Автоматизирует добавление токена ко всем ajax запросам
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//TODO!
//проверка только на цифры, 
//событие при любом изменении .. ? 
$("#check").change(function () {
    var check = $(this).val();
    var id = $(this).data('client');
    $.api.getCalculate({id: id, check: check}).then(function (data) {
        $('#decrease').text(data.decrease);
        $('#increase').text(data.increase);
    }, function (data) {
        console.log(data);
//        $.api.fail(data);
    });
});