(function ($) {
    'use strict';

    $.cddrz = $.cddrz || {};
    $.cddrz.api = (function () {

        return {
            post: post,
            get: get,
            fail: fail
        };

        function fail(err) {
            alert(err);
        }

        function any(type, url, data) {
            return $.ajax({
                url: url,
                type: type,
                data: data
            }).then(parse, ajaxFail);
        }

        function post(url, data) {
            return any('POST', url, data);
        }

        function get(url, data) {
            return any('GET', url, data);
        }

        function parse(res) {
            res = JSON.parse(res); 
            if (!res.data) {
                return $.Deferred().reject(res.err);
            }
            return res.err.msg;
        }

        function ajaxFail(err) {
            return fail({msg: err.statusText});
        }

    })();

}(jQuery));
