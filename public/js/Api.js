(function($) {
    
    $.api = (function () {

       var apiService = $.cddrz.api;
        
        return {
            getCalculate: getCalculate,
            fail: fail
        };
        
        function getCalculate(param) {
            return apiService.post('/api/calculate', param);
        }
        
        function fail(data) {
            return apiService.fail(data);
        };
        
    })();
    
}(jQuery));
