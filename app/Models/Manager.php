<?php

namespace App\Models;

class Manager extends BaseModel {

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getUser() {
        return $this->user;
    }
    
    public function transaction() {
        return $this->hasOne(Transaction::class);
    }

}
