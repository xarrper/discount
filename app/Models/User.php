<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Client;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    
    use SoftDeletes;
    
    protected $fillable = [
        'phone', 
        'name',
        'password'
    ];
    
    protected $hidden = [
        'password', 
        'remember_token'
    ];

    protected $dates = ['deleted_at'];
    
    /**
     * Получение модели клиента для пользователя.
     * Связь: один-к-одному
     * @return type
     */
    //TODO client - метод private?
    public function client() {
        return $this->hasOne(Client::class);
    }
    
    public function getClient() {
        return $this->client;
    }
    
    /**
     * Проверка, что пользователь является клиентов
     * @return type
     */
    public function issetClient() {
        return isset($this->client) ? true : false;
    }
    
    /**
     * Получение модели менеджер для пользователя.
     * @return type
     */
    //TODO manager - метод private?
    public function manager() {
        return $this->hasOne(Manager::class);
    }
    
    public function getManager() {
        return $this->manager;
    }
    
    /**
     * Проверка, что пользователь является менеджером
     * @return type
     */
    public function issetManager() {
        return isset($this->manager) ? true : false;
    }
    
}
