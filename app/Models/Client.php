<?php

namespace App\Models;

class Client extends BaseModel {
    
    public function user() {
        return $this->belongsTo(User::class); 
    }
    
    public function getUser() {
        return $this->user;
    }
    
    public function getDiscount() {
        return $this->discount;
    }
    
    public function applyDiscount($diff) {
        $this->discount = $this->getDiscount() + $diff;
    }
    
}
