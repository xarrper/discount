<?php

namespace App\Models;

class Transaction extends BaseModel {
    
    protected $fillable = [
        'client_id',
        'manager_id',
        'discount',
        'check',
        'diff'
    ];
    
}
