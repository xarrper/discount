<?php

namespace App\Models;

class Discount extends BaseModel {
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
}
