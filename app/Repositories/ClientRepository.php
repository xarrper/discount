<?php

namespace App\Repositories;

use App\Models\Client;
use App\Models\User;

class ClientRepository {//implements UserRepositoryIn)
    
    private $model;
    
    public function __construct(Client $model) {
        $this->model = $model;
    }
    
    public function findByUser(User $user) {
        return $user->getClient();
    }
    
    //TODO_ЕА: $this->model->user()->where(['phone' => $phone])->client
    public function findClientByPhone($phone) {
        return User::where(['phone' => $phone])->first()->getClient();
    }
    
    public function findClientByAttr($attr) {
        return $this->model->where($attr)->first();
    }
    
    public function findClientById($id) {
        return $this->model->find($id);
    }
    
    public function save(Client $client) {
        $client->save();
    }
    
  
}



