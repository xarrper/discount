<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository implements UserRepositoryInterface {
    
    private $model;
    
    public function __construct(User $model) {
        $this->model = $model;
    }
    
    public function find($id) {
      return $this->model->find($id);
    }
    
    public function findAttr($attr) {
      return $this->model->where($attr)->first();
    }
    
    
    
}
