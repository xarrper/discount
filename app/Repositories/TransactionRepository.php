<?php

namespace App\Repositories;

use App\Models\Transaction;

class TransactionRepository {
    
    private $model;
    
    public function __construct(Transaction $model) {
        $this->model = $model;
    }
    
    public function create($data) {
        return new Transaction($data);
    }
    
    public function save(Transaction $client) {
        $client->save();
    }
    
  
}



