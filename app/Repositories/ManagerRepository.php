<?php

namespace App\Repositories;

use App\Models\Manager;
use App\Models\User;

class ManagerRepository {
    
    private $model;
    
    public function __construct(Manager $model) {
        $this->model = $model;
    }
    
    public function findByUser(User $user) {
        return $user->getManager();
    }
    
}
