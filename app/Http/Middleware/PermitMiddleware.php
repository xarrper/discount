<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermitMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $typeUser) {
        $method = 'isset' . ucfirst($typeUser);
//        if (!Auth::user()->$method()) {
            dd(abort(403));
//        }
//        dd($next($request));
        return $next($request);
    }

}
