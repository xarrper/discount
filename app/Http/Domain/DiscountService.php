<?php

namespace App\Http\Domain;

use App\Repositories\TransactionRepository;

class DiscountService {
   
    private $transRepository;
    
    public function __construct(TransactionRepository $transRepository) {
        $this->transRepository = $transRepository;
    }
    
    /**
     * Рассчитывает колличество баллов(добавлять или отнимать), создается объект Transaction
     * @param type $client клиент 
     * @param type $check сумма
     * @param type $type true - начисляют баллы, false - снимают баллы  
     * @return type - возвращает объект Transaction
     */
    public function calculate($manager, $client, $check, $type) {
        $diff = ($type) ? $this->increase($check) : $this->decrease($client, $check);
        $transaction = $this->transRepository->create([
            'client_id' => $client->id,
            'discount' => $client->getDiscount(),
            'check' => $check,
            'diff' => $diff
        ]);
        $manager->transaction()->save($transaction);
        return $diff;
    }
    
    /**
     * Определение скидки.
     * @param type $check
     * @param type $points
     * @return type
     */
    //todo: private
    public function sale($check, $points) {
        return $sale = ($check > $points) ? $points / 2 : $check / 2; 
    }
    
    /**
     * Функция вычисления добавления количества баллов 
     * @param type $client
     * @param type $check
     * @return type
     */
    public function increase($check) {
        return ($check * config('general.percentage'));
    }
    
    /**
     * Функция вычисления отнятия количества баллов 
     * @param type $client
     * @param type $check
     * @return type
     */
    public function decrease($client, $check) {
        return -($this->sale($check, $client->discount));
    }
    
}
