<?php

namespace App\Http\Domain;

class IdentService {
   
    private $width = 150; 
    private $height = 150;
    
    /**
     * Функция генерации Ident для клиента
     * (вызывается при авторизации клиента)
     * @param type $id - id клиента
     * @return type
     */
    public function generatIdent($id) {
        return md5(time() + $id);
    }
    
    public function getQrCodeUrl($url) {
        return "https://chart.googleapis.com/chart?chs=" . $this->width . "x" . $this->height . "&cht=qr&chl=$url";
    }
}
