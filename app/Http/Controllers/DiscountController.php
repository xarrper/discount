<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\Discount;
use App\Http\Requests;

class DiscountController extends BaseController {
    
    private $model;
    
    public function __construct(Discount $model) {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * только для пользователя с ролью покупатель.
     * @param type $id
     */
    public function show($id) {
        $discount = $this->model->find($id);
        echo $discount->user->name . ' : ' . $discount->total;
    }
    
    /**
     * Добавить/отнять баллы к дисконту, в зависимости от суммы заказа.
     */
    public function add(Requests $requests) {
        $points = $this->model->find($requests->id); 
        $sum = $requests->sum;
        if ($sum > $points->total) {
            $sale = $points->total / 2;           
        }
        else {
            $sale = $sum / 2;
        }
        $points->update(['total' => $sale + $sum * 0.15]);
        echo 'Ваша скидка составляет' . $sale;            
    }

}
