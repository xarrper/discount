<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Domain\DiscountService;
use App\Repositories\ClientRepository;

class ApiController extends Controller {
    
    private $discountService;
    private $clientRepository;
    
    public function __construct(DiscountService $discountService, ClientRepository $clientRepository) {
        $this->discountService = $discountService;
        $this->clientRepository = $clientRepository;
    }
    
    public function getData($data, $errCode, $errMessage) {
        return [
            'data' => $data,
            'err' => [
                'code' => $errCode,
                'msg' => $errMessage
            ]
        ];
    }
    
    /**
     * Получить количество списаных баллов и Получить количество начисляемых баллов.
     * @param Request $request: $request->id - id пользователя; $request->check - сумма чека.
     * @return type 
     */
    //TODO: повторение кода
    public function getCalculate(Request $request) {
        $client = $this->clientRepository->findClientById($request->id);
        //todo: data - ??
        $data['decrease'] = $this->discountService->decrease($client, (int)$request->check);
        $data['increase'] = $this->discountService->increase((int)$request->check);
        return json_encode($this->getData(true, 100, $data));
    }
}
