<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Repositories\ManagerRepository;
use App\Repositories\ClientRepository;
use App\Http\Domain\DiscountService;

class ManagerController extends BaseController {
    
    private $managerRepository;
    private $clientRepository;
    private $discountService;
    private $user;
    
    public function __construct(
        ManagerRepository $managerRepository, 
        DiscountService $discountService,
        ClientRepository $clientRepository
    ) {
        parent::__construct();
        $this->managerRepository = $managerRepository;
        $this->discountService = $discountService;
        $this->clientRepository = $clientRepository;
        $this->user = Auth::user();
    }
    
    /**
     * отображение менеджера
     * @param type $id
     * @return type
     */
    public function show() {
        $manager = $this->managerRepository->findByUser($this->user);
        return view('manager.show', [
            'manager' => $manager
        ]);
    }
    
    /**
     * история менеджера
     */
    public function history() {
    }
    
    /**
     * Отображение страницы клиента для менеджера
     * @param Request $request
     * @return type
     */
    public function showClient(Request $request) {
        return view('manager.showClient', [
            'client' => $this->clientRepository->findClientById($request->id)
        ]);
    }
    
    /** 
     * Отображение дисконта для пользователя
     * @param Request $request
     * @return type
     */
    //TODO условие по телефону или по ident
    public function showDiscount(Request $request) { 
        $client = isset($request->phone) ? $this->clientRepository->findClientByPhone($request->phone)
                : $this->clientRepository->findClientById($request->id);  
        return view('discount.show', [
            'client' => $client
        ]);
    }
    
    /**
     * Расчет дисконта, при type положительном, Добавляются баллы, 15% процентов от заказа.
     * type отрицательном, баллы списываются
     * @param Request $request
     * @return type
     */
    //TODO isset($request->increase)
    public function calculateDiscount(Request $request) {
        $manager = $this->managerRepository->findByUser($this->user);
        $client = $this->clientRepository->findClientById($request->id); 
        
        $diff = $this->discountService->calculate($manager, $client, $request->check, isset($request->increase)); 
        $client->applyDiscount($diff);
        $this->clientRepository->save($client);  
        
        return redirect(route('manager.show'));
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    //TODO: делает редирект
    public function identClient(Request $request) {
        $client = $this->clientRepository->findClientByAttr(['ident' => $request->ident]);
        return redirect()->route('manager.client', ['id' => $client->id]);
    }
    
}
