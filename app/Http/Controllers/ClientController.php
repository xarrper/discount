<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Http\Domain\IdentService;
use App\Repositories\ClientRepository;

class ClientController extends BaseController {
    
    private $identService;
    private $ClientRepository;
    private $user;
    
    public function __construct(IdentService $identService, ClientRepository $ClientRepository) {
        parent::__construct();
        $this->identService = $identService;
        $this->ClientRepository = $ClientRepository;
        $this->user = Auth::user();
    }
    
    /**
     * отображение посетителя
     * @param type $id
     * @return type
     */
    public function show() { 
        $client = $this->ClientRepository->findByUser($this->user);
        return view('client.show', [
            'client' => $client,
            'qrCodeUrl' => $this->identService->getQrCodeUrl(route('manager.ident', ['ident' => $client->ident]))
        ]);
    }
    
    /**
     * история клиента
     */
    public function history() {
    }
    
}
