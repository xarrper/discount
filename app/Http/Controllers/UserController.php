<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\BaseController;

class UserController extends BaseController {    
    
    private $model;
    
    public function __construct(User $model) {
        parent::__construct();
        $this->model = $model;
    }
    
    /**
     * Вывод всех пользователей с ролью покупатель
     */
    public function index() {
        $users = $this->model->where(['role' => 5])->get();
        foreach ($users as $user) {
            echo '<a href='.route('user.show', ['id' => $user->discount->id]).'>'.$user->name.'</a></br>';
        }
    }

    public function create() {
       
    }

    public function store(Request $request) {
     
    }

    /**
     * Как делать разделение по ролям? разные контроллеры, или 
     * пока хз.
     * @param type $id
     */
    public function show($id) {
        $user = $this->model->find($id);
        if($user->role == User::ROLE_SELLER) {
            echo 'официант'; //index
            //отображение всех пользователь
            // при переходе по ним попадаем на контроллер дискаунт.
        }
        if($user->role == User::ROLE_CUSTOMER) {
            echo 'Дисконт: '.$user->discount->total;
        } 
    }

    public function edit($id) {
     
    }

    public function update(Request $request, $id) {
      
    }

    public function destroy($id) {
       
    }

}
