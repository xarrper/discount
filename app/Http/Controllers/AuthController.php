<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Domain\IdentService;
use App\Repositories\ClientRepository;

class AuthController extends BaseController {
    
    private $identService;
    private $clientRepository;
    
    public function __construct(IdentService $identService, ClientRepository $clientRepository) {
        parent::__construct();
        $this->identService = $identService;
        $this->clientRepository = $clientRepository;
    }
    
    /**
     * Проверка на авторизованость пользователя
     * выводится пароль
     * @return type
     */
    public function test() {
        $user = Auth::user();
        return view('auth.show', [
            'data' => (isset($user)) ? $user->phone : 'нет'
        ]);
    }
    
    /**
     * Авторизация пользователя
     * @param Request $request
     * @return type
     */
    //TODO - так и оставить  if ($user->issetClient()) { ?
    public function enter(Request $request) {
        Auth::loginUsingId($request->id);
        $user = Auth::user();
        if ($user->issetClient()) {
            $client = $this->clientRepository->findByUser($user);
            $client->ident = $this->identService->generatIdent($client->id);
            $this->clientRepository->save($client);
        }
        return view('auth.show', [
            'data' => 'авторизация'
        ]);
    }
    
    /**
     * Разлогинивание пользователя
     * @return type
     */
    public function exits() {
        Auth::logout();
        return view('auth.show', [
            'data' => 'выход'
        ]);
    }
    
}
