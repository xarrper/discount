<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'client', 'as' => 'client.', 'middleware' => ['permit:client']], function () {
    
        Route::get('/', [
            'as' => 'show', 'uses' => 'ClientController@show'
        ]);

        Route::get('/history', [
            'as' => 'history', 'uses' => 'ClientController@history'
        ]);
    
    });

    Route::group(['prefix' => 'manager', 'as' => 'manager.', 'middleware' => ['permit:manager']], function () {

        Route::get('/', [
            'as' => 'show', 'uses' => 'ManagerController@show'
        ]);
        
        Route::get('/history', [
            'as' => 'history', 'uses' => 'ManagerController@history'
        ]);
        
        Route::get('/client/{id}', [
            'as' => 'client', 'uses' => 'ManagerController@showClient'
        ]);
        //TODO: название и реализация метода
        Route::get('/ident/{ident}', [
            'as' => 'ident', 'uses' => 'ManagerController@identClient'
        ]);

        Route::group(['prefix' => 'discount', 'as' => 'discount.'], function () {

            Route::get('/', [
                'as' => 'show', 'uses' => 'ManagerController@showDiscount'
            ]);

            Route::post('/calculate', [
                'as' => 'calculate', 'uses' => 'ManagerController@calculateDiscount'
            ]);

        });    

    });
    
    Route::group(['prefix' => 'api', 'as' => 'api.', 'middleware' => ['permit:manager']], function () {
    
        Route::post('/calculate', [
            'as' => 'calculate', 'uses' => 'ApiController@getCalculate'
        ]);
        
    });

});


Route::get('/user', [
    'as' => 'user.index', 'uses' => 'UserController@index'
]);

Route::get('/user/{id}', [
    'as' => 'user.show', 'uses' => 'UserController@show'
]);

Route::get('/discount/{id}', [
    'as' => 'user.show', 'uses' => 'DiscountController@show'
]);

Route::post('/discount/{id}', [
    'as' => 'user.add', 'uses' => 'DiscountController@add'
]);

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    
    Route::get('/test', [
        'as' => 'test', 'uses' => 'AuthController@test'
    ]); 
    
    Route::get('/enter/{id}', [
        'as' => 'enter', 'uses' => 'AuthController@enter'
    ]); 

    Route::get('/exits', [
        'as' => 'exits', 'uses' => 'AuthController@exits'
    ]); 
});

