@extends('layouts.app')

@section('content')
    {{$client->name}} : {{$client->discount}}
    <br>
    <p>{{route('manager.ident', ['ident' => $client->ident])}}</p>
    <br>
    <img src="{{$qrCodeUrl}}">
@endsection
