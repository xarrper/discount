@extends('layouts.app')

@section('content')
    {{$client->name}} : {{$client->discount}}
    <br>
    <form role="form" action="{{route('manager.discount.calculate', [
        'id' => $client->id
    ])}}" method="POST" enctype="multipart/form-data">
         {{ csrf_field() }}
        <input type="text" name="check" id="check" data-client="{{$client->id}}">
        <br>
        Cписать <span id="decrease"></span> баллов
        <br>
        Добавить <span id="increase"></span> баллов
        <input type="submit" name="decrease" value="Списать">
        <input type="submit" name="increase" value="Добавить">
    </form>    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="{{asset('/js/common/cddrz.api.js')}}"></script>
    <script src="{{asset('/js/Api.js')}}"></script>
    <script src="{{asset('/js/app.js')}}"></script>
@endsection
